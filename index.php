<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tarea #1 - Formulario de Registro</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
    <div class="box">
        <h1 align="center">Formulario de Registro</h1>
        <form method="POST" id="form-control">
            <p>
                <label for="nombre">Nombre:</label><br>
                <input type="text" name="nombre" class="input"><br><br>

                <label for="apellido">Apellido:</label><br>
                <input type="text" name="apellido" class="input"><br><br>

                <label for="telefono">Teléfono:</label><br>
                <input type="text" name="telefono" class="input"><br><br>

                <label for="correo">Correo:</label><br>
                <input type="text" name="correo" class="input"><br><br>

                <input type="submit" value="Enviar" name="registrar" id="btnEnviar">
            </p>
        </form>
    </div>
        <?php 
        include("registro.php");
        ?>
</body>

</html>